def boxprint(sym,wid,hgt):
    if(len(sym)>1):
        raise Exception('"symbol" needs to be string of legth 1')
    if (wid < 2) or (hgt < 2):
        raise Exception('"width " and "height " sould be greater than or equal to 2')
    print (sym*wid)
    for i in range(hgt-2):
        print (sym + ' '*(wid-2) + sym)

    print (sym*wid)

boxprint('*',1,1)
boxprint('^*',20,20)
