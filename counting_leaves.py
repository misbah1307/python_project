def main():
    n = int(input())
    a = [int(i) for i in input().split(' ')]
    d = int(input())
    tree = {0:[]}
    leaf= []
    for i in range(1,n):
        if (a[i] in tree.keys()):
            tree[a[i]].append(i)
        else:
            tree.update({a[i]:[i]})
    
    #del tree[d]
    if (d in tree.keys()):
        for i in tree[d]:
            if(i in tree.keys()):
                del tree[i]
        del tree[d]

    for key in tree.keys():
        if(d in tree[key]):
            tree[key].remove(d)
        for v in tree[key]:
            if (v not in tree.keys()):
                leaf.append(v)
    print(len(leaf))






main()
