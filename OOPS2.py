class Parent:
    'This is the parent class'
    parentAttr = 100

    def __init__(self):
        print ('This is parent class constructor')

    def parentMethod(self):
        print ('Calling parent method')

    def setAttr(self,attr):
        Parent.parentAttr=attr

    def getAttr(self):
        print ('PArent attribute ==> '+str(Parent.parentAttr))



class Child(Parent):
    'This is the child class'

    def __init__(self):
        print('Calling child constructor')

    def childMethod(self):
        print ('Calling child method')


c = Child()
c.childMethod()
c.parentMethod()
c.setAttr(100)
c.getAttr()
