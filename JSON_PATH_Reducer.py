#!/usr/bin/env python3

"""
Find the path of a key / value in a JSON hierarchy easily.
It was made for JSON files, but it also works with dictionaries,
of course.
Inspired by:
* http://stackoverflow.com/a/34837235/232485 (doesn't treat nested lists)
* http://chris.photobooks.com/json/default.htm (in-browser visualization)
Author:
* Laszlo Szathmary, alias Jabba Laci, 2017, jabba.laci@gmail.com
"""

import json
import sys
import re


def traverse(path, obj):
    """
    Traverse the object recursively and print every path / value pairs.
    """

    f = open("json_objects.txt", "a")
    wrt=''
    cnt = -1
    if isinstance(obj, dict):
        d = obj
        for k, v in d.items():
            if isinstance(v, dict):
                traverse(path + "." + k, v)
            elif isinstance(v, list):
                traverse(path + "." + k, v)
            else:
                #mrt=path + "." + str(k)+ ":"+ str(v)
                #wrt=wrt+str(mrt)

                print(path + "." + k, ":", v,file=f)
    if isinstance(obj, list):
        li = obj
        for e in li:
            cnt += 1
            if isinstance(e, dict):
                traverse("{path}[{cnt}]".format(path=path, cnt=cnt), e)
            elif isinstance(e, list):
                traverse("{path}[{cnt}]".format(path=path, cnt=cnt), e)
            else:
                #wrt=wrt+str("{path}[{cnt}] : {e}".format(path=path, cnt=cnt, e=e))
                
                print("{path}[{cnt}] : {e}".format(path=path, cnt=cnt, e=e),file=f)

    f.close()


def read_file(fpath):
    """
    Read the JSON file and return its content as a Python data structure.
    """
    with open(fpath) as f:
        return json.load(f)


def process(fname):
    """
    Process the given JSON file.
    """
    d = read_file(fname)
    c = open("json_objects.txt", "r+")
    c.truncate(0)
    c.close()
    traverse("", d)

##############################################################################

if __name__ == "__main__":
    #if len(sys.argv) == 1:
     #   print("Usage: json_path <input.json>")
     #   sys.exit(1)
    #
    #fname = sys.argv[1]
    process('response.txt')

    search_list=['FROMDATE ','STARTDATE ','TILLDATE ','TODATE ','SCOPEPORTLIST ']
    f = open("json_objects.txt","r")

    w = open("params.txt","a")
    for line in f:
        for item in search_list:
            if item in line:
                linest = re.sub(r'\.instances\.(\d+)\.',r'[\1].',line.rstrip())
                linem = re.sub(r'\.([A-Za-z1-9_].*)\s:.*',r'\1',linest)
                print(linem,file=w)
    w.close()
