
def jumps_result(a1,a2,b1,b2):
    jumpa=a1
    jumpb=b1
    for i in range(0,10000):
        if(jumpa == jumpb):
            return True
        jumpa+=a2
        jumpb+=b2

    return False


numbers = list(map(int, input().split()))
a1 = numbers[0]
a2 = numbers[1]
b1 = numbers[2]
b2 = numbers[3]

if(jumps_result(a1,a2,b1,b2)):
    print ("YES")
else :
    print ("NO")
