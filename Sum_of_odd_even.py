def diff_even_odd(num_list):
    odd_sum=0
    even_sum=0
    for num in num_list:
        if(num%2 == 0):
            even_sum+=num
        else:
            odd_sum+=num
    return abs(even_sum-odd_sum)
    

def main():
    num = int(input())
    num_list = input().split(' ')
    
    for i in range(len(num_list)):
        num_list[i]=int(num_list[i])
    print(diff_even_odd(num_list))

main()
