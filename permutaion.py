# A Python program to print all 
# permutations of given length
from itertools import permutations,combinations
 


n = int(input())
A = [int(i) for i in input().split(' ')]
sumlis = []
for i in range(2,n+1):
    permlist = combinations(A,i)
    for perm in list(permlist):
        print(perm)
        sumlis.append(min(perm)|max(perm))

print(sum(sumlis)%1000000007)
