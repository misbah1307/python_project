import webbrowser, sys, pyperclip
sys.argv #contain the address entered

#check is arguments r passed
if (len(sys.argv) > 1):
    #arguments are passed
    address = ' '.join(sys.argv[1:])
else:
    address = pyperclip.paste()
#URL = https://www.google.co.in/maps/place/65,+Dr+Ambedkar+Nagar,+Kaval+Bairasandra,+Bengaluru,+Karnataka+560045/
URL = 'https://www.google.co.in/maps/place/'+address
webbrowser.open(URL)
