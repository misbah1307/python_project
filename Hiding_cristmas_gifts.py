path = {}
count = {}
lister = []
def trace_me(a,b):
    lister.append(a)
    if(lister[-1]==b):
        return
    if(len(lister)>1):
        if(lister[-1] not in path[lister[-2]]):
            del lister[-2]
    if (b in path[a]):
        lister.append(b)
        return
    for each in path[a]:
        if(each not in lister):
            trace_me(each,b)
        

def main():
    n,m = map(int,input().split(' '))
    for i in range(n-1):
        a,b=map(int,input().split(' '))
        if (a not in path.keys()):
             path.update({a:[b]})
             count.update({a:0})
        else:
            path[a].append(b)
        if (b not in path.keys()):
             path.update({b:[a]})
             count.update({b:0})
        else:
            path[b].append(a)
    for i in range(m):
        a,b = map(int,input().split(' '))
        trace_me(a,b)
        print(lister)
        for i in lister:
            count[i]+=1
        lister.clear()
        
    print(max(count.values()))  








main()    
