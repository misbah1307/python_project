
import pyautogui
import math


class DrawSpiral:
    'This is to draw circle'
    
    def __init__(self,r,x,y):
        self.dis = r
        self.co_x = x
        self.co_y = y

    def draw(self):
        pyautogui.moveTo(self.co_x,self.co_y)
        pyautogui.click()
        while self.dis!=0:
            pyautogui.dragRel(0,self.dis,duration=1)
            self.dis-=20
            pyautogui.dragRel(self.dis,0,duration=1)
            pyautogui.dragRel(0,-1*self.dis,duration=1)
            self.dis-=20
            pyautogui.dragRel(self.dis*-1,0,duration=1)
           
            
            
            


spiral =DrawSpiral(200,300,300)
spiral.draw()
    
