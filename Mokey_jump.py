###################################################
## Name :- Mohammed Misbahuddin
## Date :- 07-03-2018
###################################################


def main():

    #Take the input from stdin
    a1,b1,a2,b2=input().split(',')

    #convert it into integers (explicit casting)
    a1=int(a1)
    b1=int(b1)
    a2=int(a2)
    b2=int(b2)

    #Check if they are already at same position
    if(a1 == a2):
        print("YES")
        return
    else:
        # Always considering that a1 is behin a2.
        # If they are not so then swap the content of a1 with a2 , b1 with b2

        #Check is a1 is behind a2
        if(a1 > a2): 
            #Swapping the contents
            a1,a2=a2,a1
            b1,b2=b2,b1

        #Check if the jumping speed of a1 is less than a2
        if(b1 <= b2):

            # If jumping speed of a1 is less than a2 they can never meet
            print("NO")
            return

        #Make them jump untill a1 crosses a2 or meets a2 
        while(a1 < a2):

            #Monkeys are jumping here
            a1=a1+b1
            a2=a2+b2

            #Check if they are meeting each other
            if(a1 == a2):

                #They met eachother
                print("YES")
                return

        #They cannot meet as a1 has crossed a2
        print("NO")
        return
main()
