

def largest_even(num_list):
    new= []
    for num in num_list:
        if(num%2==0):
            new.append(num)
    return(max(new))

def main():
    num = int(input())
    num_list = input().split(' ')
    
    for i in range(len(num_list)):
        num_list[i]=int(num_list[i])
    print(largest_even(num_list))

main()
