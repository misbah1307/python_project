def main():
    result = ""
    T = int(raw_input())
    listo=[" "]*T
    for i in range(0,T):
        NumberOfProblem,P = raw_input().split(" ")
        NumberOfProblem = int(NumberOfProblem)
        P = int(P)
        S = raw_input().split(" ")
        S = [int(j) for j in S]
        result = TestCase(NumberOfProblem,P,S)
        listo[i]=result
    for i in range(0,T):
        print listo[i]


def TestCase(NumberOfProblem,P,S):
    cwd=0
    hd=0
    for i in range(0,NumberOfProblem):
        res=difficulty(P,S[i])
        if(res==0):
            cwd+=1
        if(res==1):
            hd+=1
    if((cwd==1) and (hd==2)):
        return "yes"
    else:
        return "No"

def difficulty(N,Ni):
    if(Ni>=(N/2)):
        return 0
    if(Ni<=(N/10)):
        return 1
    return -1

main()

