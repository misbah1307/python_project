def main():
    for k in range(input()):
        N = input()
        array = raw_input().split(" ")
        array = [int(array[i]) for i in range(N+1)]
        P = N-1
        result=0
        for i in range(N,0,-1):
            if(i>=2):
                for j in range(i-1,1,-1):
                    result+=(array[i]*array[j]*(2**P))
                    P-=1
                result+= (array[i]*(2**P)*(array[0]+array[1]))
                P = N-1
            else:
                result+=(array[0]*array[1]*(2**(P+1)))
        result = result %((10**9)+7)
        print result
main()
