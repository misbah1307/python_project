#! python3
import pyperclip, re
#TODO: create regc object for phoe number
phone = re.compile(r'''
(((\d\d\d) | (\(\d\d\d\)))?        #area code (optional)
(\s|-)        #frist sep
\d\d\d       #first 3 digits
-        #second sep
\d\d\d\d        #last 4 digits 
(((ext(\.)?\s)|x) #extension part
(\(d{2,5}))?)
''',re.VERBOSE)
#       for email address
email = re.compile(r'''
[a-zA-Z0-9_.+]+    #name
@     #@ symbol
[a-zA-Z0-9_.+]+    #domain 
''',re.VERBOSE)
#       get text from clipboard
text = pyperclip.paste()
#       extract email and phone from text
extracted_phone = phone.findall(text)
extracted_email = email.findall(text)
#       copy the extracted email/phone to clip board
allphoneNum = []
for ph in extracted_phone:
    allphoneNum.append(ph[0])
results = '\n'.join(allphoneNum)+ '\n' +'\n'.join(extracted_email)
pyperclip.copy(results)
