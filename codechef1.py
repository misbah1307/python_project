def find(dict):
    cw=0
    sim=0
    eas=0
    med=0
    hrd=0
    for j in range(0,len(dict)):
        strr = dict[j]
        if(strr == "cakewalk"):
            cw+=1
        if(strr == "simple"):
            sim+=1
        if(strr == "easy"):
            eas+=1
        if(strr == "easy-medium" or strr == "medium"):
            med+=1
        if(strr == "medium-hard" or strr == "hard"):
            hrd+=1
    if(cw>=1 and sim>=1 and eas>=1 and med>=1 and hrd>=1):
        return True
    return False

def main():
    dict = {}
    T = int(raw_input())
    for i in range(0,T):
        l=list()
        for j in range(0,int(raw_input())):
            l.append(raw_input())
        dict['l'+str(i)]=l
    for i in range(0,T):
        if(find(dict['l'+str(i)])):
            print "Yes"
        else:
            print "No"
main()
        
