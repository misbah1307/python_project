Print():
	end='' :- parameter that gets appended at end of print function
	sep='' :- separator that gets added on every for each param
	
List methods :-
	index :- gives index of the value
	append() :- adds value at the end of list
	insert :- insert at specific index in list
	remove :- removes specific value from list { del list[index]}
	sort() :- sorts the list in ascending order{sort(reverse=True) :- sorts in descending order}
	sort(key=str.lower) :- sort in true alphabetoical order
	
	"copy" module is used to create a copy of the list
	chese = copy.deepcopy(spam)
	
	
	use "\" to nullify the indentation check
	print ("askjgd   kjsadfjgsv "+ \
			"SADGAAAJH")
			
Dictionaries:-
	key value pair
	have no order 
	in and not in operator can be used 
	variables hold the dictinaried refferences
	methods:-
		dict.keys() :- return keys
		dict.values() :- return values
		dict.items() :- return items ad ('key','value')
		dict.get() :- return the value of the passed key , if not exist it reeturn the second argument passed
		dict.setdefault() :- set the default value if the key doesnt exist in the dict
	
	
String :- 
	slices and indices can be used 
	Methods : methods do not change values of string instead they return new string
		string.upper() :- returns string with upper chracters
		string.lower() :- returns string wiht lowers case characters
		string.islower() :- checks if string has only lower case and returns bolean
		string.isupper() :- 
			  .isalpha() :- true if str contains only letters
			  .isalnum() :- true if str contain only numbers and letters
			  .isdecimal() :- true for numbers only
			  .isspace() :- true for blank spaces only
			  .istitle() :- true for if all words in str begins with upper and followes by all lower case
			  .startswith(param) :- true if str starts with that param
			  .endswith(param) ;- true if ends with that param
			  ','.join() :- takes list as parameter and joins the elements of list with , as delimeter
			  str.split() :- split the string into list with passed value as delimeter . default is space delimeter
			  str.rjust(len) :- inserts (len-str) spaces(or second argument if passed) before str to make its total length = len
			  str.ljust(len) :- inserts (len-str) spaces(or second argument if passed) after str to make its total length =len
			  str.centre(len) :- alighn str in centre making total length = len
			  str.strip() :- remove off the spaces  or passed (characters in any order)string from pre and  post position in the string
			  str.lstrip() :- remove off spaces or passed (characters in any order)string from left
			  str.rstrip() :- remove off spaces or passed (characters in any order)string from right
			  str.replace() :- replace existence of first param to second 
	string formatting example :
		'hi %s can u come to $s' % (name,address)


#############################################################################################################################################
Classes and objects in python :-
	__init__(self,x,y) :- is like parametrized constructor in python. initialization of instances are added under this method.
	__add__() :- __<>__ is used for special methos like operator overloading etc.. 
	hasattr(<instance>,<attribute>) :- It returns true if the passed attribute is part of the current instance.
	getattr(<instance>,<attribute>) :- It returns the value of the attribute of the instance passed.
	setattr(<instance>,<attribute>,<value>) :- It sets the attribute value of the passed instance.
	delattr(<instance>,<attribute>) :- It deletes the attribute of the instance.
	
	
	Built-in class attributes:-
		__dict__ :- dictionary containing class namespace.
		__doc__ :- class documentaion string or None is not defined.
		__name__:- contains the class name.
		__module__ :-Module name in which the class is defined
		__bases__ :- A possibly empty tuple containing the base classes, in the order of their occurrence in the base class list.
		
	Inheritance :-
		issubclass(<subClassName>,<superClassName>) :- returns true if true.
		isinstance(<object>,<class>) :- returns true is obj is instance of that class.
		
	An attribute starting with __name is hidden and is not visible to outside world. it can be accesed only via class method.
	

#############################################################################################################################################

		
FILES HANDLING ;-
	open(<path>):- open file in read mode plain text files retuns the file object. Second argument defines the mode <'w' :- write , 'a' :- append > (is file doesnt exist it creates the file)
	FIleObj.read():- read the contents of the file and returns it as string
	FileObj.readlines():-returns the list where each item is single line
	FileObj.close():- closes the file
	FileObj.write(<contents>):- returns the size been written to file. write method doesnt not directly add newline character at end we need to add manually	
		
		
		
MODULES :

pprint :-
	its a module that prints the dict in clean format
	pprint function helps in doing that 
	pformat function returns string of the pprint value 
	
	
sys :-
	argv:- it contains the passed command line arguments . It is list which can be accesed usually like a list
	
re :- regular expression module
	re.complie() :- we need to pass the raw string
	.search :- returns object matches that re matches with the passed string
	.findall :- gives the list that contains all the accurence of the pattern in a passed string as param
				match object has group method which returns the group we can also create groups for the re
	REGULAR expressions :-
				regular expression does a greedy match that is it matches the longest possible match
				in order to perform non-greedy match  need to specify {}? "? after {}" 
				
		character classes :-
			\d := digits
			\D := not A NUMERIC didgit from 0-9
			\w := matches letter, numeric,and _
			\W := matches character whcih is not letter, numeric,or _
			\s := space,tab or newline
			\Sthat not space,tab or newline
			use [] to specify our own character classes
			
	. :- matches everything other than newline
		with second character as re.DOTALL make it match everything
	re.IGNORECASE or re.I:- this will ignore the case and does case insensitive matching
	
	re.VERBOSE :- used inside re.compile 

	
os :- https://hplgit.github.io/edu/ostasks/ostasks.pdf
	os.path.join(<list>) :- concatenates the list items and makes it as a  path of the file for respective os
	os.sep :- returns the separator according to the os
	os.getcwd() :- this function returns the current working directory
	os.chdir(<dir path>) :- it changes the current working directory
	os.path.abspath(<filename>) :- absolute path of file can be specified here . the fikle name given will be appended to current working directory
	os.path.isabs(<path>):- True if path is absolute and false if not
	os.path.relpath(<F1/F2/F3>,<F1>):- will return realtive path between two paths given (seconde to first) . Here it returns "F2/F3"
	os.path.dirname(<path to file>):- it will return path till last directory in path given
	os.path.basename(<path to file>):- it will return the file name only or last folder only if file name is not provided
	os.path.exists(<path to file>):- returns true if file exits in os else false
	os.path.isfile(<path to file>):- returns true is its a path to file . returns false if its path to directory
	os.path.isdir(<path to dir>);- retrurns true if its a directory else false
	os.path.getsize(<path to file>):- returns the size of file in bytes as int
	os.listdir(<path to folder>):- return the list of files and folders in given folder
	os.makedirs(<list of folders path>):- creates these folders in respective abs or rel path given , it can also create folders inside another folders
	

	
shelve :- 
	its module that enables us creates a file which can be written like a dictionary and list format
	shleve.open(<dataFile>):- it will create file in binary format not human readable . It returns a shelveObj
	shelveObj['cats'] = ['masj','askhhgvfd','asjkfk']
	shelveObj['cats']
	==> # It will print the contents of that peticulat key
	shelveObj.keys():- returns the keys in the shelve file
	shelveObj.values():- return the values lists (converts it into list )
	shelveObj.close()
	
logging :-
	help logging debug info 
	{
		import logging
		logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s'):- configuration for logs
		logging.basicConfig(filename = 'myfile.log',level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s'):- logs in file myfile.log
	}
	logging.debug()
	logging.info()
	logging.warning()
	logging.error()
	logging.critical()
	logging.disable(logging.CRITICAL): disables all log below critical level and including itself
	
webbrowser :-
	webbrowser.open(URL) :- this will launch the new browser for the specified URL
	
requests :- http://requests.readthedocs.org/en/latest
			This module is used to download the file using the URL . This will return the response object, 
			status _code attribute tells the status of download
			text attribute contains the complete downloaded content.
			raise_for_status returns any exception while downloading, gibe details about what went vwrong.
			iter_content(len) will return the content of downloaded file of length len
			writing into file , need to open a file in binary mode i.e open('myFile.txt','wb') 
			
BeautifulSoup :-
			import bs4,
			obj = bs4.Beautifulsoup(res.text,"html.parser"):- res is the response object of the request module
			elem = obj.select('css selector')
			elem[0].text.strip() :- this returns the extracted text from the webpage
			
selenium :- http://elenium-python.readthedocs.org
			It is the m odule used to automate the browser to perform the things we want.
			from selenium import webdriver
			brow = webdriver.Firefox(executable_path='/Users/mohammedmisbahuddin/Downloads/geckodriver-v0.19.1-win64/geckodriver.exe'):- It will open the firefox browser window
			brow.get('URL'):- it will open up the windows in firefox browser with the specified URL
			elem =  brow.find_element_by_css_selector(CSS_SELECTOR):- it will return the obj of the element 
			elem =  brow.find_elements_by_css_selector(CSS_SELECTOR):- it will return the multiple obj of the css selector passed 
			elem.click():- it will simulate the clicking of the elemnet in webpage.
			brow.find_elements_by_css_selector(CSS_SELECTOR)
			brow.find_element_by_css_selector(CSS_SELECTOR)
			
			brow.find_element_by_class_name(class_name)
			brow.find_elements_by_class_name(class_name)
			
			brow.find_element_by_id(id)
			
			brow.find_element_by_link_text(text) :- <a> elements  that completely matches the text provided
			
			brow.find_element_by_partial_link_text(text) :- <a> elements that contains the text provided
			
			brow.find_element_by_name(name) :- elements matching the name attriobute value.
			
			brow.find_element_by_tag_name(text) :- elements with matching tag name.
			
			
			elem.send_keys('text') :- here the element is the textbox. send_keys method writes the text on to that text box of the html form.
			elem.submit() :- here it finds the submit buttong associated with the elem and simulates its ckick.
			
			
openpyxl :- module used tt work with xl sheets etc...
			openpyxl.load_workbook('filename path') :- iot returns the workbook object .
			workbook.get_sheet_by_name('sheet name') :- it returns the sheet with corresponding name.
			workbook.get_sheet_names() :- it returns the sheet names of the workbook.
			sheet['<columnName><rowNumber>'] :- it returns the cell object corresponding to that index.
			sheet.cell[row=<rowNumber>,column=<colNumber>] ;- it will also return the cell object.
			cell.value :- it returns the value in that cell object.
			
			openpyxl.workbook() :- this will create a worklbook object .
			workbook.create_sheet() :- adds new sheet and returns the sheet object.
			sheet.title = 'sheet title' :- This is used to rename the sheet.
			workbook.create_sheet(index=<index_value>, title=<sheet_title>) ;- this will create sheet at speciofied index, with specified title.
			workbook.save('filename.xlsx'):- This will save the workboook with specified filename in cwd.
			
PyPDF2 :- Module used to read the text from the pdf file.
		import PyPDF2
		pdfFile = open(<filename>,'rb') :- open the pdf file in read binary mode.
		Reader = PyPDF2.PdfFileReader(pdfFile) :- after passing the opened pdffile , it returns the reader object from the PyPDF2.PdfFileReader function.
		Reader.numPages :- gives u the number of pages in pdf file.
		page = Reader.getPage(index<0-n>):- it will return the page object of the specified index number.
		page.extractText() :- This will extract text and will be returned.
		
		
		
		{noformat}
				pdfFile2 =open('meetingminutes2.pdf','rb')
		>>> pdfFile1 = open('meetingminutes1.pdf','rb')
		>>> reader1 = PyPDF2.PdfFileReader(pdfFile1)
		>>> reader2 = PyPDF2.PdfFileReader(pdfFile2)
		>>> writer = PyPDF2.PdfFileWriter()
		>>> for numPage in range(reader1.nmuPages):
			currPage = reader1.getPage(numPage)
			writer.addPage(currPage)

			
		Traceback (most recent call last):
		  File "<pyshell#220>", line 1, in <module>
			for numPage in range(reader1.nmuPages):
		AttributeError: 'PdfFileReader' object has no attribute 'nmuPages'
		>>> for numPage in range(reader1.numPages):
			currPage = reader1.getPage(numPage)
			writer.addPage(currPage)

			
		>>> for numPage in range(reader2.numPages):
			currPage = reader2.getPage(numPage)
			writer.addPage(currPage)

			
		>>> outFile = open('combinedminutes.pdf','wb')
		>>> writer.write(outFile)
		>>> outFile.close()
		>>> pdfFile.close()
		>>> pdfFile1.close()
		>>> pdfFile2.close()
		{noformat}
		
		
python-docx :- http://python-docx.readthedocs.org
			doc = docx.Document(filename) :- this will openn the filename in that path given
			each document contain a paragraphs , and each paragraphs contain runs(sentences till the next style changes).
			doc.paragraphs:- will contain the list of paragraphs,
			p = doc.paragraphs[index] :- will contain the paragraph at that index, .text methiod returns the text at that paragraphs.
			p.style :- contain the style (normal, italic,bold......)
			p.runs[index].bold :- bolean value set to true if text is bold at that run index.
			p.runs[index].italic :- bolean.
			p.runs[index].underline :- bolean.
			p.runs[index].text :- contain the text at that run.
			doc.add_paragraph('para') :- this method adds paragraph to the document.
			p.add_run('text'):- adds run to the paragraph.
			doc.save(filename) :- saves the doc in the specified filename.
			
smtplib :- module for mail tranfer.
		conn= smtplib.SMTP('smtp.gmail.com',587)
		conn.ehlo() :- to start connection.
		conn.starttls() :- start encryption.
		conn.login(<username>,<password>) :- 
		conn.sendmail(from,to,body) :- sendmail
		conn.quit() :- quit connection .
			
			

			