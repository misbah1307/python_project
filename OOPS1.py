class Employee(object):
    'This is the documentation string of the class Employee'
    empCount = 0

    def __init__(self,Name,Salary):
        self.name=Name
        self.salary=Salary
        Employee.empCount+=1


    def getEmployeeCount():
        return Employee.empCount;

    def getEmployeeDetails(self):
        print ("Name ==> "+self.name+"\n Salary ==> "+str(self.salary)+"\n\n")



E1= Employee('Mohammed',50000)
print("\n\nEmployee Count ====> "+str(Employee.empCount))
E1.getEmployeeDetails()
E2 = Employee('Misbahuddin',100000)
print("\n\nEmployee Count ====> "+str(Employee.empCount))
E2.getEmployeeDetails()
