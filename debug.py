import logging
logging.basicConfig(filename='myfilelog.txt',level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
#logging.disable(logging.CRITICAL )#this will not allow the debug msg to be printed
logging.debug('start of program')

def factorial(n):
    logging.info('start of factorial')
    total =1
    for i in range(1,n+1):
        total *=i
        logging.error('i is %s, total is %s' %(i,total))
    logging.critical('return value is %s' %total)
    return total

print (factorial(5))
logging.debug('end of program')
