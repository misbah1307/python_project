def unity(num_list):
    new_list = []
    zeros = num_list.count(0)
    for num in num_list:
        if(num!=0):
            new_list.append(num)
    for i in range(zeros):
        new_list.append(0)
    return new_list


def main():
    num = int(input())
    num_list = input().split(' ')
    
    for i in range(len(num_list)):
        num_list[i]=int(num_list[i])

    for num in unity(num_list):
        print(str(num),end=' ')
    

main()
